package com.prudkij.operations;

import java.util.List;

/**
 * Is an math multiply operation
 * 
 * @author Kirill
 *
 */
public class OperationMultiply extends Operations {

	/**
	 * Saves operation symbol, priority and operands amount saving
	 */
	public OperationMultiply() {
		super("*", 2, 2);
	}

	/**
	 * Returns the result of multiplying elements from {@code numbers}
	 * 
	 * @param numbers - list with operands
	 * @return operation solving
	 */
	@Override
	public double calculate(List<Double> numbers) {
		return numbers.get(0) * numbers.get(1);
	}

}
