package com.prudkij.operations;

import java.util.List;

/**
 * Is an math subtraction operation
 * 
 * @author Kirill
 *
 */
public class OperationSubtraction extends Operations {

	/**
	 * Saves operation symbol, priority and operands amount saving
	 */
	public OperationSubtraction() {
		super("-", 1, 2);
	}

	/**
	 * Returns the result of subtracting elements from {@code numbers}
	 * 
	 * @param numbers - list with operands
	 * @return operation solving
	 */
	@Override
	public double calculate(List<Double> numbers) {
		return numbers.get(0) - numbers.get(1);
	}

}
