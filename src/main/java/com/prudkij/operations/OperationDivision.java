package com.prudkij.operations;

import java.util.List;

/**
 * Is an math division operation
 * 
 * @author Kirill
 *
 */
public class OperationDivision extends Operations {

	/**
	 * Saves operation symbol, priority and operands amount saving
	 */
	public OperationDivision() {
		super("/", 2, 2);
	}

	/**
	 * Returns the result of dividing elements from {@code numbers}
	 * 
	 * @param numbers - list with operands
	 * @return operation solving
	 */
	@Override
	public double calculate(List<Double> numbers) {
		return numbers.get(0) / numbers.get(1);
	}

}
