package com.prudkij.operations;

import java.util.List;

/**
 * Defines common behaviors for math operations objects
 * 
 * @author Kirill
 *
 */
public abstract class Operations {
	protected String symbol = "";
	protected int priority = 0, operandsCapacity = 0;

	/**
	 * Saves information about operation
	 * 
	 * @param symbol           - math symbol
	 * @param priority         - operation priority
	 * @param operandsCapacity - operands amount
	 */
	protected Operations(String symbol, int priority, int operandsCapacity) {
		this.symbol = symbol;
		this.priority = priority;
		this.operandsCapacity = operandsCapacity;
	}

	/**
	 * Returns math symbol of definite math operation
	 * 
	 * @return math symbol of operation
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Gives operation priority
	 * 
	 * @return operation priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * Returns operands amount
	 * 
	 * @return operands amount
	 */
	public int getOperandsCapacity() {
		return operandsCapacity;
	}

	/**
	 * Calculates elements of {@code numbers}
	 * 
	 * @param numbers - list with numbers for calculation.
	 * @return result of calculating
	 */
	public abstract double calculate(List<Double> numbers);
}
