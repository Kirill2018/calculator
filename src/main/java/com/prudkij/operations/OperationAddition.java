package com.prudkij.operations;

import java.util.List;

/**
 * Is an math addition operation
 * 
 * @author Kirill
 *
 */
public class OperationAddition extends Operations {

	/**
	 * Saves operation symbol, priority and operands amount saving
	 */
	public OperationAddition() {
		super("+", 1, 2);
	}

	/**
	 * Returns the result of adding elements from {@code numbers}
	 * 
	 * @param numbers - list with operands
	 * @return operation solving
	 */
	@Override
	public double calculate(List<Double> numbers) {
		return numbers.get(0) + numbers.get(1);
	}

}
