package com.prudkij.service;

/**
 * The {@code DataManager} interface should be implemented by any class which
 * instances are intended to administrate and share information among
 * several services
 * 
 * @author Kirill
 */
public interface DataManager {
	/**
	 * Distributes information among several operations. Returns the result of
	 * operations
	 * 
	 * @param expression - information for distributing
	 * @return solution from services
	 * @throws Exception if one of the operations gets crushed
	 */
	String manage(String expression) throws Exception;
}