package com.prudkij.service.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.prudkij.exceptions.ValidationException;
import com.prudkij.operations.Operations;
import com.prudkij.service.ASCIIConstants;

/**
 * Validates expression for calculator
 * 
 * @author Kirill
 *
 */
public class ExpressionValidator implements Validatable {

	private final String MSG_INVALID_LENGTH_EXPRESSION = "Invalid length of expression. Expression length must be more then one symbol";
	private final String MSG_UNKNOWN_OPERATION = "Unknown math operation";
	private final String MSG_INVALID_OPERANDS_CAPACITY = "Invalid operands capacity. Operands number does not match the number of operations";
	private final String MSG_INVALID_BRACKETS_CAPACITY = "Expression contains invalid number of brackets.";
	private final String MSG_INVALID_SYMBOL_NEAR_BRACKETS = "expression elements near brackets are invalid. Near brackets must be operation symbols or bracket";

	private Pattern patternMathSybolsAndNumbers = null;
	private Pattern patternAllExpression = null;

	private List<Operations> operations = null;

	/**
	 * Initializes patterns
	 * 
	 * @param operations - list with all possible maths operations
	 */
	public ExpressionValidator(List<Operations> operations) {
		this.operations = operations;

		String mathSybolsAndNumbersStr = "(?<nums>\\d+)|(?<symbols>";
		for (Operations operation : operations) {
			mathSybolsAndNumbersStr += "\\" + operation.getSymbol() + "|";
		}
		patternMathSybolsAndNumbers = Pattern
				.compile(mathSybolsAndNumbersStr.substring(0, mathSybolsAndNumbersStr.length() - 1) + ")");
		patternAllExpression = Pattern.compile("("
				+ mathSybolsAndNumbersStr.substring(0, mathSybolsAndNumbersStr.length() - 1) + ")|\\s|\\(|\\)){2,}");

	}

	/**
	 * {@code validatingExpression} checks a length, operands quantity, brackets
	 * quantity, right math operations near brackets and right math
	 * symbols as well.
	 * 
	 * @param validatingExpression - expression for validating
	 * @return {@code true} if and only if data is valid or generates ValidationException
	 * @throws ValidationException with message if {@code validatingExpression} is not possible
	 * 
	 */
	public boolean validate(String validatingExpression) throws ValidationException {
		// checking for a length
		if (validatingExpression.length() < 2) {
			throw new ValidationException(MSG_INVALID_LENGTH_EXPRESSION);
		}

		// checking for a right operations and operands count
		int operandsCapacity = 1;
		Matcher allExpresionMatcher = patternAllExpression.matcher(validatingExpression);
		if (allExpresionMatcher.matches()) {
			Matcher symbolsAndNumbersMatcher = patternMathSybolsAndNumbers.matcher(validatingExpression);
			while (symbolsAndNumbersMatcher.find()) {
				if (symbolsAndNumbersMatcher.group("nums") != null) {
					operandsCapacity--;
				} else {
					for (Operations operation : operations) {
						if (operation.getSymbol().equals(symbolsAndNumbersMatcher.group())) {
							operandsCapacity++;
							break;
						}
					}
				}
			}
		} else {
			throw new ValidationException(MSG_UNKNOWN_OPERATION);
		}

		if (operandsCapacity != 0) {
			throw new ValidationException(MSG_INVALID_OPERANDS_CAPACITY);
		}

		// checking for a number of brackets
		if (validatingExpression.startsWith(ASCIIConstants.CLOSE_BRACKET)
				|| validatingExpression.endsWith(ASCIIConstants.OPEN_BRACKET)) {
			throw new ValidationException(MSG_INVALID_BRACKETS_CAPACITY);
		}
		int bracketsCapacity = 0;
		if (validatingExpression.startsWith(ASCIIConstants.OPEN_BRACKET)) {
			bracketsCapacity++;
		}
		if (validatingExpression.endsWith(ASCIIConstants.CLOSE_BRACKET)) {
			bracketsCapacity--;
		}

		char[] arrayExpression = validatingExpression.toCharArray();
		ArrayList<String> symbolsNearBreacks = new ArrayList<String>();
		for (int i = 1; i < arrayExpression.length - 1; i++) {
			String strValue = String.valueOf(arrayExpression[i]);
			if (ASCIIConstants.OPEN_BRACKET.equals(strValue)) {
				bracketsCapacity++;
				symbolsNearBreacks.add(String.valueOf(arrayExpression[i - 1]));
			} else if (ASCIIConstants.CLOSE_BRACKET.equals(strValue)) {
				bracketsCapacity--;
				symbolsNearBreacks.add(String.valueOf(arrayExpression[i + 1]));
			}
		}
		if (bracketsCapacity != 0) {
			throw new ValidationException(MSG_INVALID_BRACKETS_CAPACITY);
		} else {
			// checking for a right symbols near brackets
			for (String el : symbolsNearBreacks) {
				if (ASCIIConstants.OPEN_BRACKET.equals(el) || ASCIIConstants.CLOSE_BRACKET.equals(el)) {
					continue;
				}
				boolean isSymbolValid = false;
				for (Operations operation : operations) {
					if (operation.getSymbol().contains(el)) {
						isSymbolValid = true;
						break;
					}
				}
				if (!isSymbolValid) {
					throw new ValidationException(MSG_INVALID_SYMBOL_NEAR_BRACKETS);
				}
			}
		}

		return true;
	}
}
