package com.prudkij.service.validator;

import com.prudkij.exceptions.ValidationException;

/**
 * Validates information
 * 
 * @author Kirill
 *
 */
public interface Validatable {
	/**
	 * Validates {@code validatingExpression}
	 * 
	 * @param validatingExpression - information for validation
	 * @return {@code true} if and only if info is valid
	 * @throws ValidationException if it fails
	 */
	boolean validate(String validatingExpression) throws ValidationException;
}
