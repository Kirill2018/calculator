package com.prudkij.service;

/**
 * ASCII constants storage
 * 
 * @author Kirill
 *
 */
public interface ASCIIConstants {
	public final int INDEX_OF_NULL = 48;
	public final int INDEX_OF_NINE = 57;
	public final String OPEN_BRACKET = "(";
	public final String CLOSE_BRACKET = ")";
	public final char SPACE = ' ';
}
