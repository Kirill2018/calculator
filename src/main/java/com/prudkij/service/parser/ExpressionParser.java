package com.prudkij.service.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.prudkij.operations.Operations;
import com.prudkij.service.ASCIIConstants;

/**
 * Parses expression to a right example for calculating
 * 
 * @author Kirill
 *
 */
public class ExpressionParser implements Parser {
	private Pattern numsPattern = null;

	/**
	 * Initializes pattern
	 */
	public ExpressionParser() {
		numsPattern = Pattern.compile("\\d+");
	}

	/**
	 * Returns parsed expression without spaces. Expression is divided by
	 * operands and math symbols. For correct parsing expression must be validated
	 * 
	 * @param expression - validated expression for parsing
	 * @param operations - list with all possible math operations
	 * @return list with parsed expression. In this list each element is a number or
	 *         math operation
	 */
	@Override
	public List<String> parse(String expression, List<Operations> operations) {
		ArrayList<String> answer = new ArrayList<String>();
		Matcher matcher = numsPattern.matcher(expression);
		int start = 0;
		while (matcher.find()) {
			String symbol = "";
			int end = matcher.start();
			if (end - start != 0) {
				for (int i = start; i < end; i++) {
					symbol += expression.charAt(i);
					for (Operations operation : operations) {
						if (operation.getSymbol().equals(symbol)) {
							answer.add(symbol);
							symbol = "";
							break;
						}
					}
					if (symbol.equals(ASCIIConstants.OPEN_BRACKET) || symbol.equals(ASCIIConstants.CLOSE_BRACKET)) {
						answer.add(symbol);
						symbol = "";
					}
				}
				if (symbol.length() != 0) {
					break;
				}
			}
			answer.add(matcher.group());
			start = matcher.end();
		}

		if (start != expression.length()) {
			answer.add(String.valueOf(expression.charAt(expression.length() - 1)));
		}

		return answer;
	}
}
