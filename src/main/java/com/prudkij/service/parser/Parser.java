package com.prudkij.service.parser;

import java.util.List;

import com.prudkij.operations.Operations;

/**
 * Corrects the expression according to the requirements of the definite
 * calculator
 * 
 * @author Kirill
 *
 */
public interface Parser {
	/**
	 * Realizations should transform expression for further calculating
	 * 
	 * @param expression - string sequence for parsing
	 * @param operations - list with possible maths operations for correct parsing
	 * @return list with parsed expression
	 */
	List<String> parse(String expression, List<Operations> operations);
}
