package com.prudkij.service.calculator;

import java.util.List;

import com.prudkij.operations.Operations;

/**
 * Implemented instances should calculate the expression
 * 
 * @author Kirill
 *
 */
public interface Calculatable {
	/**
	 * Calculates {@code calculatingExpression} and returns solving.
	 * 
	 * @param calculatingExpression - expression for calculating
	 * @param operations            - list with all possible math operations
	 * @return solving a math example
	 */
	double calculate(List<String> calculatingExpression, List<Operations> operations);
}
