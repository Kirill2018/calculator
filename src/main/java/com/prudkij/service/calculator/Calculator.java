package com.prudkij.service.calculator;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import com.prudkij.operations.Operations;
import com.prudkij.service.ASCIIConstants;
import com.prudkij.service.parser.ExpressionParser;

/**
 * Calculates math expressions.
 * 
 * @author Kirill
 *
 */
public class Calculator implements Calculatable {
	/**
	 * Calculates {@code calculatingExpression} with the help of reverse polish sequence.
	 * Expression with brackets calculates recursing. {@code calculatingExpression}
	 * should be validated. Each element of it should contain operand or math
	 * operation symbol. Expression can be parsed to a right example using
	 * {@link ExpressionParser}. It can calculate expressions only with math operations
	 * stored in {@code operations}
	 * 
	 * @param calculatingExpression - validated and parsed expression for
	 *                              calculating
	 * @param operations            - list with all possible math operations
	 * @return math example solving
	 */
	public double calculate(List<String> calculatingExpression, List<Operations> operations) {
		Stack<Double> numbersStack = new Stack<Double>();
		Stack<Operations> operationsStack = new Stack<Operations>();
		LinkedList<Double> numbersForCalculate = new LinkedList<Double>();

		for (int elementIndex = 0; elementIndex < calculatingExpression.size(); elementIndex++) {
			String expressionElement = calculatingExpression.get(elementIndex);
			// Block for brackets expression. Method is call recursed with expression into
			// brackets
			if (expressionElement.charAt(0) >= ASCIIConstants.INDEX_OF_NULL
					&& expressionElement.charAt(0) <= ASCIIConstants.INDEX_OF_NINE) {
				numbersStack.add(Double.parseDouble(expressionElement));
				continue;
			} else if (ASCIIConstants.OPEN_BRACKET.equals(expressionElement)) {
				boolean openBracket = false;
				for (int elementInBraketIndex = elementIndex + 1; elementInBraketIndex < calculatingExpression
						.size(); elementInBraketIndex++) {
					String expressionInBraketElement = calculatingExpression.get(elementInBraketIndex);
					if (ASCIIConstants.OPEN_BRACKET.equals(expressionInBraketElement)) {
						openBracket = true;
					} else if (ASCIIConstants.CLOSE_BRACKET.equals(expressionInBraketElement)) {
						if (openBracket) {
							openBracket = false;
						} else {
							numbersStack.add(calculate(
									calculatingExpression.subList(elementIndex + 1, elementInBraketIndex), operations));
							elementIndex = elementInBraketIndex;
							break;
						}
					}
				}
				continue;
			}
			// If expression element is operation - it is puted to operations stack. Last
			// operation in stack called if
			// priority of current operation high than priority of last operation in stack
			for (Operations operation : operations) {
				if (operation.getSymbol().equals(expressionElement)) {
					// ------------------------------------------
					if (operationsStack.isEmpty() || operationsStack.peek().getPriority() < operation.getPriority()) {
						operationsStack.add(operation);
						break;
					}
					while (operationsStack.size() > 0
							&& operationsStack.peek().getPriority() >= operation.getPriority()) {
						for (int i = 0; i < operationsStack.peek().getOperandsCapacity(); i++) {
							numbersForCalculate.addFirst(numbersStack.pop());
						}
						numbersStack.add(operationsStack.pop().calculate(numbersForCalculate));
						numbersForCalculate.clear();
					}
					operationsStack.add(operation);
					break;
				}
			}
		}
		// Last iterate over the operation stack
		for (int i = operationsStack.size() - 1; i >= 0; i--) {
			Operations operation = operationsStack.pop();
			for (int j = 0; j < operation.getOperandsCapacity(); j++) {
				numbersForCalculate.addFirst(numbersStack.pop());
			}
			numbersStack.add(operation.calculate(numbersForCalculate));
			numbersForCalculate.clear();
		}

		return numbersStack.pop();
	}
}
