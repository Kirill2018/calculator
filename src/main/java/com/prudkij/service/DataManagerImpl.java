package com.prudkij.service;

import java.util.ArrayList;
import java.util.List;

import com.prudkij.exceptions.ValidationException;
import com.prudkij.operations.OperationAddition;
import com.prudkij.operations.OperationDivision;
import com.prudkij.operations.OperationMultiply;
import com.prudkij.operations.OperationSubtraction;
import com.prudkij.operations.Operations;
import com.prudkij.service.calculator.Calculatable;
import com.prudkij.service.calculator.Calculator;
import com.prudkij.service.parser.ExpressionParser;
import com.prudkij.service.validator.ExpressionValidator;
import com.prudkij.service.validator.Validatable;

/**
 * This manager sends information from user for verification, transformation for
 * calculating and calculation itself
 * 
 * @author Kirill
 *
 */
public class DataManagerImpl implements DataManager {
	private List<Operations> operations = new ArrayList<Operations>();

	/**
	 * Initializes maths operations
	 */
	public DataManagerImpl() {
		operations.add(new OperationMultiply());
		operations.add(new OperationSubtraction());
		operations.add(new OperationDivision());
		operations.add(new OperationAddition());
	}

	/**
	 * {@code expression} is validated, parsed and given to the calculator. After that answer is given in string form.
	 * 
	 * @param expression - string request from user
	 * @return string representation of the solving a math example or generate ValidationException
	 * @throws ValidationException if validation fails
	 */
	public String manage(String expression) throws ValidationException {
		ExpressionParser parser = new ExpressionParser();
		Validatable validator = new ExpressionValidator(operations);
		Calculatable calculator = new Calculator();
		if (validator.validate(expression)) {
			return Double.toString(calculator.calculate(parser.parse(expression, operations), operations));
		}
		return "";
	}
}
