package com.prudkij.service.history;

import java.util.List;
import java.util.Set;

/**
 * Stores math expressions
 * 
 * @author Kirill
 *
 */
public interface History {
	/**
	 * Gives away all expressions
	 * 
	 * @return list with all expressions
	 */
	List<String> getHistory();

	/**
	 * Returns only unique expressions
	 * 
	 * @return set with only unique expressions
	 */
	Set<String> getUniqueHistory();

	/**
	 * Saves new expression
	 * 
	 * @param expression - information for saving
	 */
	void setHistory(String expression);
}
