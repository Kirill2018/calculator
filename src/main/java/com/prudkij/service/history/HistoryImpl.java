package com.prudkij.service.history;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Stores math expressions
 * 
 * @author Kirill
 *
 */
public class HistoryImpl implements History {
	private List<String> history = new ArrayList<String>();

	/**
	 * Gives all expressions
	 * 
	 * @return list with all expressions
	 */
	@Override
	public List<String> getHistory() {
		return new ArrayList<String>(history);
	}

	/**
	 * Returns only unique expressions
	 * 
	 * @return set with unique expressions
	 */
	@Override
	public Set<String> getUniqueHistory() {
		return new HashSet<String>(history);
	}

	/**
	 * Saves new expression to {@code history}
	 * 
	 * @param expression - string expression for saving
	 */
	@Override
	public void setHistory(String expression) {
		history.add(expression);
	}
}
