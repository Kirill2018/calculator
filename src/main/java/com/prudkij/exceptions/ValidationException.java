package com.prudkij.exceptions;

/**
 * Thrown when {@link com.sysgears.prudkij.service.validators.Validatable}
 * subclasses failed validation
 * 
 * @author Kirill
 *
 */
public class ValidationException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new exception with no detail message.
	 */
	public ValidationException() {
	}

	/**
	 * Constructs a {@code ValidationException} with the specified detail message.
	 * 
	 * @param msg the detail message.
	 */
	public ValidationException(String msg) {
		super(msg);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * 
	 * @param message the detail message.
	 * @param cause   the cause where {@code null} value is permitted, and indicates
	 *                that the cause is nonexistent or unknown
	 */
	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new exception with the specified cause.
	 * 
	 * @param cause the cause where {@code null} value is permitted, and indicates
	 *              that the cause is nonexistent or unknown
	 */
	public ValidationException(Throwable cause) {
		super(cause);
	}
}
