package com.prudkij.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.prudkij.service.DataManager;
import com.prudkij.service.history.History;
import com.prudkij.service.history.HistoryImpl;

/**
 * Communicates with the user via the console
 * 
 * @author Kirill
 */
public class ConsoleUI implements UI {

	private final String MSG_FOR_USER = "Write your expression. If you want to see a history list - write 'history' - to see all requests or 'history unique' - to see unique requests. "
			+ "Fot complete the programm write 'exit'";

	/**
	 * Takes info from user and checks it for matching with commands. After that it
	 * exchanges information with history or gives away information to manager
	 * 
	 * @param manager - object for processing of user`s request
	 */
	public void start(DataManager manager) {
		BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
		History history = new HistoryImpl();
		while (true) {
			System.out.println(MSG_FOR_USER);
			try {
				String userRequest = consoleReader.readLine().toLowerCase();

				if (UICommands.EXIT.getCommandMessage().equals(userRequest)) {
					break;
				} else if (UICommands.REQUEST_HISTORY.getCommandMessage().equals(userRequest)) {
					System.out.println("All expression:\n");
					history.getHistory().forEach(expression -> {
						System.out.println(expression);
					});
				} else if (UICommands.REQUEST_UNIQUE_HISTORY.getCommandMessage().equals(userRequest)) {
					System.out.println("Unique expression:\n");
					history.getUniqueHistory().forEach(expression -> {
						System.out.println(expression);
					});
				} else {
					String answerForUser = userRequest.replaceAll(" ", "") + "=" + manager.manage(userRequest) + "\n";
					System.out.println(answerForUser);
					history.setHistory(answerForUser);

				}
			} catch (IOException e) {
				System.out.println("Reading from console exception");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}
}
