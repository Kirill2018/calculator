package com.prudkij.ui;

/**
 * It is storage with commands for user interfaces
 * 
 * @author Kirill
 *
 */
public enum UICommands {
	REQUEST_HISTORY("history"), REQUEST_UNIQUE_HISTORY("history unique"), EXIT("exit");

	private final String commandMessage;

	/**
	 * Constructs new command object with detail message
	 * 
	 * @param commandMessage - detail message
	 */
	UICommands(String commandMessage) {
		this.commandMessage = commandMessage;
	}

	/**
	 * Returns command message
	 * 
	 * @return command message
	 */
	public String getCommandMessage() {
		return commandMessage;
	}
}
