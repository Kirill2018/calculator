package com.prudkij.ui;

import com.prudkij.service.DataManager;

/**
 * The root interface in the user interface hierarchy. User interfaces are the point
 * for communicating with the user
 * 
 * @author Kirill
 *
 */
public interface UI {
	/**
	 * Provides communication with the user and transfers information from the user
	 * for processing
	 * 
	 * @param manager - object for processing of user`s request
	 */
	void start(DataManager manager);
}
