package com.prudkij;

import com.prudkij.service.DataManagerImpl;
import com.prudkij.ui.ConsoleUI;
import com.prudkij.ui.UI;

/**
 * Is start point of program.
 * 
 * @author Kirill
 *
 */
public class Main {
	public static void main(String[] args) {
		try {
			UI ui = new ConsoleUI();
			ui.start(new DataManagerImpl());
		} catch (Throwable e) {
			System.out.println("Unexpected exception");
		}
	}
}
